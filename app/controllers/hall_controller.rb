class HallController < ApplicationController
    
    def show
        @users_by_points = User.order(points: :desc).all
        @users_by_streak = User.order(longest_streak: :desc).all
    end
end
