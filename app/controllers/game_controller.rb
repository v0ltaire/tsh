class GameController < ApplicationController
   include UsersHelper
   include GameHelper
    
    respond_to :html, :js
    
    def play
    end
    
    
    def resolve
        if params[:chosen] == 'game_over'
            session[:lives] = 0
        else
            @computer_choice = $VAR_HANDS.sample
            @winning_hand = $RULES[params[:chosen].to_sym][@computer_choice.to_sym]
            calc_game_status(claim_winner)
        end
        respond_to do |format|
            if game_over?
            #clear session, save score in database and render summary page
                @user = User.new(name: session[:user_name], points: session[:user_points], longest_streak: session[:user_longest_streak])
                @user.save
                session.clear
                format.js { render :partial => "shared/game_over.js"}
            else
                format.js { render :partial => "shared/update.js"}
            end
        end
    end
    
end
