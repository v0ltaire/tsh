class UsersController < ApplicationController
  
  include UsersHelper
  
  def new
    @user = User.new
  end
  
  def show
    @user = User.find(params[:id])
  end
  
  def create
      @user = User.new(name: params[:user][:name], points: 0, longest_streak: 0)
      if @user.validate
        log_in @user
        redirect_to play_path
      else
        render 'new'
      end

  end
  
  
end
