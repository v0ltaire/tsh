class User < ActiveRecord::Base
    
    validates :name, presence: true, length: {maximum: 30 }
    validates :points, presence: true
    validates :longest_streak, presence: true
end
