module UsersHelper
    
    def log_in(user)
        session[:user_id] = user.id
        session[:user_name] = user.name
        session[:user_points] = user.points
        session[:user_longest_streak] = user.longest_streak
        session[:lives] = 3;
        session[:user_current_streak] = 0
    end
  
    def logged_in?
        !session[:user_name].nil?
    end
  
end
