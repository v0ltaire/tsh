module GameHelper
    
    $VAR_HANDS = %w[spock scissors lizard rock paper]
    $RULES =  {
            :rock     => {:rock => :draw, :paper => :paper, :scissors => :rock, :lizard => :rock, :spock => :spock},
            :paper    => {:rock => :paper, :paper => :draw, :scissors => :scissors, :lizard => :lizard, :spock => :paper},
            :scissors => {:rock => :rock, :paper => :scissors, :scissors => :draw, :lizard => :scissors, :spock => :spock},
            :spock    => {:rock => :spock, :paper => :paper, :scissors => :spock, :lizard => :lizard, :spock => :draw},
            :lizard   => {:rock => :rock, :paper => :lizard, :scissors => :scissors, :lizard => :draw, :spock => :lizard}
        }
        
    def calc_game_status (winner)
        case winner
            when :computer
                session[:lives] -= 1
                session[:user_current_streak] = 0
            when :player
                session[:user_points] += 1
                session[:user_current_streak] += 1
                if session[:user_current_streak] > session[:user_longest_streak]
                    session[:user_longest_streak] = session[:user_current_streak]
                end
        end
    end
    
    def game_over?
        case session[:lives]
            when 0 then true
            else false
        end
    end
    
    def claim_winner
        @winner = case @winning_hand
            when @computer_choice.to_sym then :computer
            when :draw then :draw
            else :player
        end
    end
    
end
