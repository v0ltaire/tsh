require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @user = User.new(name: "Test", points: 50, longest_streak: 5)
  end

  test "should be valid" do
    assert @user.valid?
  end
  
  test "all values should be present" do
    @user.name = "     "
    @user.points = nil
    @user.longest_streak = nil
    assert_not @user.valid?
  end
  
  test "name must not be too long" do
    @user.name = "a" * 31
    assert_not @user.valid?
  end
end
