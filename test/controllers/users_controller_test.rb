require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
  end
  
  test "should redirect to play" do
    post :create, :user => {name: "abc", points: 0, longest_streak: 0 }
    assert_redirected_to :play
  end
  
end
 