class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.integer :points
      t.integer :longest_streak

      t.timestamps null: false
    end
  end
end
